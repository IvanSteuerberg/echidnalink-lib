const express = require('express');
const cors = require('cors')
const app = express();
const { EchidnaBoard } = require('./EchidnaBoard.js');

function startLink(tray) {
    app.use(cors());
    const server = require('http').createServer(app);

    const io = require('socket.io')(server);

    let board = new EchidnaBoard.EchidnaBoard(io, tray, server);

    const port = process.env.PORT || 3030;

    server.listen(port);
    console.log(`Server listening on http://localhost:${port}`);

    return server;
}

exports.echidnalink = { startLink }