# Echidnalink library

LearningML (https://learningml.org) is an educational platform intended to learn Artificial Intelligence fundamentals. LearningML is offered in two ways: as a web application (https://learningml.org/editor) or as a desktop application (https://web.learningml.org/learningml-desktop). 

Echidna STEAM (https://echidna.es) is an educational project offering an MCU Board based on arduino nano.

EchidnaScratch is a Scratch modification with new blocks intended to program echidna board with firmata (http://firmata.org/wiki/Main_Page) as firmware.

Echidnalink (https://gitlab.com/juandalibaba/echidnalink) is the application allowing echidna boards communicate with EchidnaScratch. Since EchidnaScratch runs inside the sandboxed environment of a web broser, direct communication with serial port isn't allowed. Therefore echidnalink opens a websocket allowing the communication between the web browser and the serial port.

The desktop flawor of LearningML (https://web.learningml.org/learningml-desktop) have incorporated an echidnalink inside its code, so that users can make Scratch programs combining Artificial Intelligence and echidna boards.

This repo is intended to house the common code shared with echidnalink and LearningML-Desktop.

## NPM package

The library can be installed from npm:

```bash
npm install echidna-lib
```